package no.uib.ii.inf112.pond;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.pond.impl.PositionImpl;

public class PositionTest {

    Position position;

    @BeforeEach
    void setUp(){
        position = Position.create(1, 2);
    }

    @Test
    void testGetXAndY() {
        assertEquals(1, position.getX());
        assertEquals(2, position.getY());
        
    }
    @Test
    void testSet(){
        position.setX(3);
        position.setY(4);
        assertEquals(3,position.getX());
        assertEquals(4,position.getY());
    }

    @Test
    void testMove() {
        position.move(5, 6);
        assertEquals(6  , position.getX());
        assertEquals(8, position.getY());
        
    }
}
