package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {
		

		public String center(String text, int width) {
			
			int extra = (width - text.length()) / 2;
			
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = width - text.length();
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = width - text.length();
			return text + " ".repeat(extra)  ;
		}

		public String justify(String text, int width) {
			
				String[] words = text.split(" ");
				int spaces = words.length - 1;
				int totalLength = 0;
				for (String word : words) {
				  totalLength += word.length();
				}
				int extra = width - totalLength;
				if (extra <= 0) {
				  return text;
				}
				int spacesPerWord = extra / spaces;
				int additionalSpaces = extra % spaces;
				StringBuilder result = new StringBuilder();
				for (int i = 0; i < words.length ; i++) {
				  result.append(words[i]);
				  if (i < words.length - 1 ) {
					int numspaces = spacesPerWord;
					if (additionalSpaces > 0) {
					  numspaces++;
					  additionalSpaces--;
					}
					for (int j = 0; j < numspaces; j++) {
					  result.append(" ");
					}
				  }
				}
				return result.toString();
			  }
		};

	

		
		
	@Test
	void testLength() {
		assertTrue(aligner.center("Foo", 5).length() == 5);
		assertTrue(aligner.flushLeft("Foo", 5).length() == 5);
		System.out.println(aligner.flushRight("Foo", 5));
		assertTrue(aligner.flushRight("Foo", 5).length() == 5);
	}

	@Test
	void testFlushRight() {
		assertTrue(aligner.flushRight("Foo", 5).equals("  Foo"));
	}

	@Test
	void testFlushLeft() {
		assertTrue(aligner.flushLeft("Foo", 5).equals("Foo  "));
	}
	@Test
	void testCenter() {
	
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
	@Test
	void testJusify(){
		assertEquals("fee   fie  foo", aligner.justify("fee fie foo", 14));
	}
}
